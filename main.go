package main

var (
	Quartz  QuartzController
	CNA     SonyCNA1
	PushTop PushTopRouter
	config  Config
)

func init() {
	config = getConfig()
	setupLogs(config)
	routeCh := make(chan Route)
	interogateCh := make(chan InterogateRequest)

	Quartz = QuartzController{
		Port:          config.Quartz.ListenPort,
		InterogateChn: interogateCh,
		RoutetChn:     routeCh,
	}

	CNA = SonyCNA1{
		Username:      config.Sony.Username,
		Password:      config.Sony.Password,
		IpAddress:     config.Sony.IpAddress,
		InterogateChn: interogateCh,
		RoutetChn:     routeCh,
	}

	PushTop = PushTopRouter{
		Port:      config.PushTop.Port,
		IpAddress: config.PushTop.IpAddress,
	}
}

func main() {
	if config.PushTop.Enabled {
		go PushTop.init()
	}
	go CNA.HandleRequests()
	Quartz.Init()
	Quartz.Listen()
}
