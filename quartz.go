package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
)

type QuartzController struct {
	Port          string
	InterogateChn chan InterogateRequest
	RoutetChn     chan Route
	listner       net.Listener
	clients       map[string]net.Conn
}

func (Q *QuartzController) Init() {
	Q.clients = make(map[string]net.Conn)
}

func (Q *QuartzController) Listen() {
	var err error

	Q.listner, err = net.Listen("tcp4", ":"+Q.Port)
	if err != nil {
		log.Printf("Failed to start listening for clients %s", err)
		return
	}

	for {
		conn, err := Q.listner.Accept()
		if err != nil {
			log.Printf("Failed to start new connection with client: %s", err)
			continue
		}
		log.Printf("Adding new client to the conneciton list: %s", conn.RemoteAddr().String())
		Q.clients[conn.RemoteAddr().String()] = conn
		go Q.clientHandle(conn.RemoteAddr().String())
	}
}

func (Q *QuartzController) clientHandle(connId string) {
	conn := Q.clients[connId]
	reader := bufio.NewReader(conn)

	for {
		msg, err := reader.ReadString('\r')
		if err != nil {
			log.Printf("Failed to read data from client at %s: %s", conn.RemoteAddr().String(), err.Error())
			Q.closeClientConn(connId)
			return
		}
		msg = strings.TrimSuffix(msg, "\r")
		switch msg[:2] {
		case ".I":
			log.Printf("Received interrogate message %s from client %s", msg, conn.RemoteAddr().String())
			dest, err := strconv.Atoi(msg[3:])
			if err != nil {
				log.Printf("Failed to convert destination number to int: %s", err.Error())
				continue
			}

			request := InterogateRequest{
				ClientId:    connId,
				Level:       msg[2],
				Destination: dest,
			}

			Q.InterogateChn <- request

		case ".S":
			log.Printf("Received route message %s from client %s", msg, conn.RemoteAddr().String())
			split := strings.Split(msg[3:], ",")
			dest, err := strconv.Atoi(split[0])
			if err != nil {
				log.Printf("Failed to convert destination number to int: %s", err.Error())
				continue
			}

			source, err := strconv.Atoi(split[1])
			if err != nil {
				log.Printf("Failed to convert source number to int: %s", err.Error())
				continue
			}

			request := Route{
				ClientId:    connId,
				Level:       msg[2],
				Destination: dest,
				Source:      source,
			}

			Q.RoutetChn <- request

		case ".#":
			if msg[2:4] == "01" {
				conn.Write([]byte(".A\r"))
			}
		default:
			conn.Write([]byte(".E\r"))
			log.Printf("Received from %s unsupported message: %s", conn.RemoteAddr().String(), msg)

		}
	}
}

func (Q *QuartzController) closeClientConn(connId string) {
	log.Printf("Closing connection with client: %s", connId)
	conn := Q.clients[connId]
	conn.Close()
	delete(Q.clients, connId)
}

func (Q *QuartzController) BroadcastRouteUpdate(route Route) {
	update := fmt.Sprintf(".U%c%d,%d\r", route.Level, route.Destination, route.Source)
	for client, conn := range Q.clients {
		_, err := conn.Write([]byte(update))
		if err != nil {
			log.Printf("Failed to send data to client %s: ", err)
		} else {
			log.Printf("Sending route update %s to client %s.", update[:len(update)-1], client)
		}
	}
}

func (Q *QuartzController) SendRouteUpdate(route Route) {
	update := fmt.Sprintf(".A%c%d,%d\r", route.Level, route.Destination, route.Source)

	_, err := Q.clients[route.ClientId].Write([]byte(update))
	if err != nil {
		log.Printf("Failed to send data to client %s: ", err)
	} else {
		log.Printf("Sending route update %s to client %s.", update[:len(update)-1], route.ClientId)
	}
}

func (Q *QuartzController) SendError(connId string) {
	Q.clients[connId].Write([]byte(`.E`))
}
