package main

type InterogateRequest struct {
	ClientId    string
	Level       byte
	Destination int
}

type Route struct {
	ClientId    string
	Level       byte
	Destination int
	Source      int
}

type Config struct {
	Quartz struct {
		ListenPort  string `json:"listen_port"`
		UnassignNum int    `json:"unassigned_source_num"`
	} `json:"quartz"`

	Sony struct {
		Username  string `json:"username"`
		Password  string `json:"password"`
		IpAddress string `json:"ip_address"`
	} `json:"sony"`

	Logging struct {
		Path string `json:"path"`
	} `json:"logging"`
	PushTop struct {
		Enabled   bool   `json:"enabled"`
		IpAddress string `json:"ip_address"`
		Port      string `json:"port"`
	} `json:"push_top"`
}
