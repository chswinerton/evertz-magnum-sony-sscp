package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/bobziuchkovski/digest"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strconv"
)

type SonyCNA1 struct {
	IpAddress     string
	Username      string
	Password      string
	InterogateChn chan InterogateRequest
	RoutetChn     chan Route
	conn          net.Conn
	reader        *bufio.Reader
}

func (S *SonyCNA1) HandleRequests() {
	for {
		select {
		case req := <-S.InterogateChn:
			go S.Interrogate(req)
		case req := <-S.RoutetChn:
			go S.Route(req)
		}
	}
}

func (S *SonyCNA1) Interrogate(req InterogateRequest) {
	log.Printf("Requesting the route status of RCP %d", req.Destination)
	var status [][]int
	client := digest.NewTransport(S.Username, S.Password)
	request, err := http.NewRequest("GET", fmt.Sprintf("http://%s/admin/rcp_assign_status?request=rcp_status", S.IpAddress), nil)
	if err != nil {
		log.Printf("Failed to handle interrogate request: %s", err.Error())
		Quartz.SendError(req.ClientId)
		return
	}

	request.SetBasicAuth(S.Username, S.Password)

	resp, err := client.RoundTrip(request)
	if err != nil {
		log.Printf("Failed to handle interrogate request: %s", err.Error())
		Quartz.SendError(req.ClientId)
		return
	}

	defer resp.Body.Close()
	statusBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Failed to handle interrogate request: %s", err.Error())
		Quartz.SendError(req.ClientId)
		return
	}
	err = json.Unmarshal(statusBytes, &status)
	if err != nil {
		log.Printf("Failed to handle interrogate request: %s", err.Error())
		Quartz.SendError(req.ClientId)
		return
	}

	source := status[req.Destination-1][0]

	if source == 0 {
		source = config.Quartz.UnassignNum
	}

	update := Route{
		ClientId:    req.ClientId,
		Level:       'V',
		Destination: req.Destination,
		Source:      source,
	}

	go Quartz.SendRouteUpdate(update)
}

func (S *SonyCNA1) Route(req Route) {
	log.Printf("Requesting CCU %d be routed to RCP %d.", req.Source, req.Destination)
	source := strconv.Itoa(req.Source)
	if req.Source == config.Quartz.UnassignNum {
		source = "0"
	}
	destination := strconv.Itoa(req.Destination)

	client := digest.NewTransport(S.Username, S.Password)
	request, err := http.NewRequest("GET", fmt.Sprintf("http://%s/admin/rcp_assign_config?asgn_chg=%s:%s", S.IpAddress, destination, source), nil)
	if err != nil {
		log.Printf("Failed to handle interrogate request: %s", err.Error())
		Quartz.SendError(req.ClientId)
		return
	}
	request.SetBasicAuth(S.Username, S.Password)

	resp, err := client.RoundTrip(request)
	if err != nil {
		log.Printf("Failed to handle interrogate request: %s", err.Error())
		Quartz.SendError(req.ClientId)
		return
	}

	defer resp.Body.Close()

	update := Route{
		ClientId:    req.ClientId,
		Level:       'V',
		Destination: req.Destination,
		Source:      req.Source,
	}

	if config.PushTop.Enabled {

		PushTop.route(update)

	}

	go Quartz.BroadcastRouteUpdate(update)
}
