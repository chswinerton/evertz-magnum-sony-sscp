package main

import (
	"encoding/json"
	"github.com/lestrrat-go/file-rotatelogs"
	"io"
	"io/ioutil"
	"log"
	"os"
	"time"
)

func getConfig() Config {
	var config Config
	data, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Fatalf("Failed to read config from file config.json: %s", err.Error())
	}
	err = json.Unmarshal(data, &config)
	if err != nil {
		log.Fatalf("Failed to read config from file config.json: %s", err.Error())
	}
	return config
}

func setupLogs(config Config) {
	filePath := config.Logging.Path + "rcp_assign_%Y%m%d%H.log"
	logHandle, err := rotatelogs.New(filePath, rotatelogs.WithMaxAge(72*time.Hour), rotatelogs.WithRotationTime(8*time.Hour))
	if err != nil {
		log.Printf("failed to create rotatelogs: %s", err)
		return
	}

	multiWriter := io.MultiWriter(os.Stdout, logHandle)
	log.SetOutput(multiWriter)
}
