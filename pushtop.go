package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

type PushTopRouter struct {
	conn      net.Conn
	reader    *bufio.Reader
	Port      string
	IpAddress string
}

func (c *PushTopRouter) init() {
	err := c.connect()
	if err != nil {
		c.reconnect()
	}
	go c.handleMessages()
	go c.heartbeat()
}

func (c *PushTopRouter) connect() error {
	var err error
	c.conn, err = net.Dial("tcp", c.IpAddress+":"+c.Port)
	if err != nil {
		return err
	}
	c.reader = bufio.NewReader(c.conn)
	return nil
}

func (c *PushTopRouter) handleMessages() {
	for {
		msg, err := c.reader.ReadString('\r')
		if err != nil {
			log.Printf("Failed to read data from router: %s", err.Error())
			c.reconnect()
			return
		}
		msg = strings.TrimSuffix(msg, "\r")
		switch msg[:2] {
		case ".E":
			log.Printf("Received error message from rotuer: %s", msg)
		case ".A":
			log.Printf("Recieved upate mesage from router: %s", msg)
		default:
			log.Printf("Received unsupported message from router: %s", msg)

		}

	}
}

func (c *PushTopRouter) heartbeat() {
	for {
		c.conn.Write([]byte(".#01\r"))
		time.Sleep(5 * time.Second)
	}
}

func (c *PushTopRouter) reconnect() {
	for {
		err := c.connect()
		if err != nil {
			log.Printf("Failed to reconnect to router at: %s", c.IpAddress)
			time.Sleep(10 * time.Second)
		}
		break
	}
}

func (c *PushTopRouter) route(route Route) {
	req := fmt.Sprintf(".S%c%d,%d\r", route.Level, route.Destination, route.Source)

	_, err := c.conn.Write([]byte(req))
	if err != nil {
		log.Printf("Failed failed to make RCP pushtop route request %s: %s ", req, err.Error())
	} else {
		log.Printf("Sending RCP pushtop route request: %s", req)
	}
}
